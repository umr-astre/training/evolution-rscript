# The evolution of the R script

Going from a analysis script, through a RMarkdown report, up to a _pipeline_-structured analysis with [`targets`](https://wlandau.github.io/targets/).

Explore the commit history to witness the changes step by step.
