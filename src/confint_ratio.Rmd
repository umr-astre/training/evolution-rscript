---
title: "Confidence Interval for a ratio"
author: "Facundo Muñoz"
date: "`r format(Sys.Date(), '%e %B, %Y')`"
output:
  bookdown::pdf_document2:
    template: null
documentclass: cirad
bibliography: confidence_intervals.bib
editor_options: 
  markdown: 
    wrap: 72
---

```{r eval = interactive(), cache = FALSE, include = FALSE}
source("src/packages.R")
source("src/functions.R")
message("Assuming interactive session")
```

```{r setup, include=FALSE, cache = FALSE}
knitr::opts_chunk$set(
  echo = FALSE,
  cache = FALSE,
  dev = ifelse(is_latex_output(), "cairo_pdf", "png"),
  dpi = 300
)

theme_set(theme_ipsum(grid = "Y"))
```

# Introduction

```{r load, include = FALSE}
tar_load(
  c(
    clean_data,
    sim_data,
    sim_pars,
    raw_data
  )
)
```

We surveyed 2 variables ($X$ and $Y$ counts) from a population and we
are interested in their ratio $Y/X$. We want to make inference on the
**average ratio** in the population.

We need to make sure that $X > 0$ to avoid infinities in the ratio.
Typically, we survey one variable ($Y$) given non-negative values of the
other. So, choose the variables accordingly. Specifically, we survey
households inhabited by at least one person, and conditioned to that, we
count the number of dogs. Not the converse.


\clearpage

# Data description

```{r data-description, fig.cap = cap}
cap <- "Sample distributions of survey data (dog-human ratio, number of humans and number of dogs) by zone."
clean_data |> 
  pivot_longer(
    Nbh:dh_ratio,
    names_to = "variable",
    values_to = "value"
  ) |> 
  ggplot(aes(value)) +
  geom_histogram(bins = 15) +
  facet_grid(zone ~ variable, scales = "free")
```


# Method 1: classical CI for a population mean

This is the most standard and classical method, which is based on
asymptotic normality of the sample mean of any distribution
[@wang_confidence_2001].

Let $R = Y/X$ be the quantity of interest. Consider the sample mean
$\bar R = \sum_{i=1}^n r_i/n$ and variance
$S^2 = \sum_{i=1}^n (r_i - \bar R)^2 / (n-1)$.

The theory says that, when $n \to \infty$,
$(\bar R - \mu_R) / (S/\sqrt{n}) \sim t_{n-1}$. Thus, a confidence
interval for the population mean $\mu_R$ is $$
  \bar R \pm t_{\alpha/2}\,S/\sqrt{n}
$$ where $1-\alpha$ is the confidence level and $t_{\alpha/2}$ is the
upper tail $\alpha/2$ percentile of the Student $t$ distribution with
$n-1$ degrees of freedom.

```{r confint-ratio-normal, echo = TRUE}
confint_ratio_normal <- function(x, alpha = 0.05) {
  hatx <- mean(x)
  s2 <- var(x)
  n <- length(x)
  ta2 <- qt(alpha/2, n - 1, lower.tail = FALSE)
  
  return(hatx + ta2*sqrt(s2/n) * c(-1, 1))
}
```

```{r res-normal}
# res_normal <- 
#   bind_rows(
#     clean_data |>
#       select(zone, y = dh_ratio) |> 
#       add_column(dataset = "Real", .before = 1),
#     sim_data |>
#       select(zone, y = r) |> 
#       add_column(dataset = "Simulated", .before = 1)
#   ) |> 
#   group_by(dataset, zone) |> 
res_normal <- 
  clean_data |>
  select(zone, y = dh_ratio) |> 
  group_by(zone) |> 
  summarise(
    Mean = mean(y),
    Variance = var(y),
    CI95_a = confint_ratio_normal(y)[1],
    CI95_b = confint_ratio_normal(y)[2],
    .groups = "drop"
  )
```

```{r table-confint-normal}
cap <- "Results using the Normal asymptotic approximation."
kbl(
  res_normal,
  booktabs = TRUE,
  digits = 3,
  caption = cap
)
```

If all is required are rough estimates of the average dog-human ratio in urban and rural areas, this method is good enough.

However, we can not make inference about the __difference__ of the urban and rural ratios.
In particular, drawing conclusions from the overlap in the confidence intervals is incorrect.
For that we need something else, like bootstrap estimates.


# Method 2: Bootstrapping

The _bootstrap_ is a non-parametric computational approach based on resampling with replacement the observed data in order to simulate a large number of replications of the experiment.

There are several variations of the method for calculating confidence intervals. I will use the basic version here.


```{r confint-ratio-boot, echo = TRUE}
get_estimates <- function(x, i) {
  i_u <- i[x$zone[i] == "URBAIN"]
  i_r <- i[x$zone[i] == "RURAL"]
  m_u <- mean(x$dh_ratio[i_u])
  m_r <- mean(x$dh_ratio[i_r])
  c(RURAL = m_r, URBAIN = m_u, `U-R` = m_u - m_r)
}
clean_data_boot <- boot(clean_data, get_estimates, R = 1e4)
```


```{r res-boot}
res_boot <- 
  clean_data_boot$t0 |> 
  enframe(name = "zone", value = "Mean") |> 
  left_join(
    map_dfr(
      setNames(seq_along(clean_data_boot$t0), names(clean_data_boot$t0)),
      ~ boot.ci(clean_data_boot, index = ., type = "basic")$basic[1, 4:5]
    ) |> 
      add_column(side = c("CI95_a", "CI95_b")) |> 
      pivot_longer(
        cols = -side,
        names_to = "zone",
        values_to = "value"
      ) |> 
      pivot_wider(
        names_from = "side",
        values_from = "value"
      ),
    by = "zone"
  )

```

```{r table-confint-boot}
cap <- "Results using the basic Bootstrap method."
kbl(
  res_boot,
  booktabs = TRUE,
  digits = 3,
  caption = cap
)
```

We get similar confidence intervals for the dog-human ratio in rural and urban areas.
A little downward shift, specially in rural areas.
But more importantly, we get inference on the difference, of which we can say that is of an order of magnitude smaller and non-significant.


Yet, if we need something _more_ than the population mean, like predictive statements such as _what is the chance of a household having more dogs than expected, or the chance of having at least a dog_, then we need a proper model.


# Method 3: Statistical modelling

Since we are working with counts, the simplest model is Poisson.

\begin{equation}
  \begin{aligned}
  \label{eq:model}
    Y_i & \sim \text{Po}(X_i\cdot\lambda_i) \\
    \lambda_i & = \beta_0 + \beta_U \mathbb{I}_U
  \end{aligned}
\end{equation}
were $\mathbb{I}_U$ is an indicator variable of urban area.

In this model, the number of dogs $Y_i$ in a household $i$ is a Poisson
random variable, with mean proportional to the number of human
inhabitants $X_i$ with a proportionality constant $\lambda_i$, which is
the parameter of interest. This represents the average dog-human ratio,
and depends on the zone (rural/urban) of the household.

Specifically, $\lambda_R = \beta_0$ is the average dog-human log-ratio
in urban areas, and $\lambda_U = \beta_0 + \beta_U$ is the average
dog-human log-ratio in rural areas.


```{r fm1}
fm1 <- glm(
  formula = Ndog ~ zone,
  family = "poisson",
  offset = clean_data$Nbh,
  data = clean_data
)
```

```{r fm1-summary}
summary(fm1)
```


```{r table-estimates-model}
exp(c(coef(fm1), sum(coef(fm1)))[c(1, 3, 2)]) |> 
  setNames(c("RURAL", "URBAIN", "U/R")) |> 
  enframe(name = "zone", value = "Mean")
```

This is giving much smaller ratios, and a factor of 6 in favour of urban areas.
Note that we no longer talk of differences but of relative factor, as a consequence of the model formulation.

Computing confidence intervals here would involve the use of the Bootstrap again, but using the model estimates instead of empirical averages.
However, there is evidence of over-dispersion in the data, so a more appropriate model should be first developed in order to continue the analysis.

<!-- \clearpage -->

<!-- # Simulation study -->


<!-- ```{r sim-pars} -->
<!-- cap <- "Parameters used for simulating data." -->
<!-- sim_pars |>  -->
<!--   kbl( -->
<!--     booktabs = TRUE, -->
<!--     caption = cap -->
<!--   ) -->
<!-- ``` -->

<!-- In order to evaluate the accuracy of the alternative methods, I simulated data from model \@ref(eq:model) with parameters (Table \@ref(tab:sim-pars)) that mimic the real observed data (Figure \@ref(fig:sim-data-description)).  -->


<!-- ```{r sim-data-description, fig.cap = cap} -->
<!-- cap <- "Sample distributions of __simulated__ survey data (dog-human ratio (r), number of humans (x) and number of dogs (y)) by zone." -->
<!-- sim_data |>  -->
<!--   pivot_longer( -->
<!--     x:r, -->
<!--     names_to = "variable", -->
<!--     values_to = "value" -->
<!--   ) |>  -->
<!--   ggplot(aes(value)) + -->
<!--   geom_histogram(bins = 15) + -->
<!--   facet_grid(zone ~ variable, scales = "free") -->
<!-- ``` -->



# Conclusions

If simple confidence intervals in urban and rural areas are required for reporting, the Normal approximation will suffice.

If the difference between urban and rural ratios is of interest as well, then a Bootstrap method can be used.

However, if the actual interest is on the number of dogs that can be expected in a household, then a proper statistical model is needed.
In particular, modelling reveals that considering the difference of ratios can be misleading since it is determined in part by the distribution of the number of inhabitants in urban and rural areas.
It might make more sense to consider the ratios instead.


# References
